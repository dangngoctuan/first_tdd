require 'rails_helper'
RSpec.describe Text, :type => :model do
    context 'association' do
      it { should belong_to(:article) }
    end
    context 'validation' do
      it { should validate_presence_of(:text_head_line) }
      it { should validate_presence_of(:text_menu) }
      it { should validate_presence_of(:message) }
      it {should validate_presence_of(:text_likes) }
    end
    before do
      @text = create(:text)
    end
    it 'method increment show likes' do
      expect(@text.increment).to eq 10
    end
end
