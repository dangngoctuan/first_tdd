require 'rails_helper'
RSpec.describe Article, :type => :model do
    context 'association' do
      it { should have_many(:texts) }
      it { should have_many(:images) }
    end
    context 'validation' do
      it { should validate_presence_of(:title) }
      it { should validate_presence_of(:article_likes) }
    end
    context ' method of aricle ' do
      before do
        @art = create(:article)
        @text = create(:text)
        @text1 = create(:text, text_menu: 2)
        @text2= create(:text, text_menu: 3)
        @img = create(:image)
        @img1 = create(:image, image_menu: 3)
        @img2 = create(:image, image_menu: 4)
      end
      it 'find article have image, text ' do
        expect(@art.by_search).to eq [@text,@text1,@text2,@img,@img1,@img2]
      end
      it 'method respond published?' do
        expect(@art.by_published).to eq true
      end
      it ' datetime published < time.now' do
        expect(@art.by_published_day).to eq true
      end
      it 'self article public succesfull' do
        expect(@art.by_public).to eq @art.to_s
      end

      it 'method increment show likes' do
        expect(@art.increment).to eq 10
      end

    end

end
