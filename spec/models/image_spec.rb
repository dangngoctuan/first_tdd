require 'rails_helper'
RSpec.describe Image, :type => :model do
    context 'association' do
      it { should belong_to(:article) }
    end
    context 'validation' do
      it { should validate_presence_of(:image_head_line) }
      it { should validate_presence_of(:image_menu) }
      it { should validate_presence_of(:url) }
      it { should validate_presence_of(:image_likes) }
    end
    before do
      @img = create(:image)
    end
    it 'method increment show likes' do
      expect(@img.increment).to eq 15
    end
end
