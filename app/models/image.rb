class Image < ActiveRecord::Base
  default_scope {order(image_menu: :asc)}
  belongs_to :article, optional: true
  validates :image_head_line, :image_menu, :url, :image_likes, presence: true
  def increment
    self.image_likes
  end
end
