class Text < ActiveRecord::Base
  default_scope {order(text_menu: :asc)}
  belongs_to :article, optional: true
  validates :text_head_line, :text_menu, :message, :text_likes, presence: true
  def increment
    self.text_likes
  end
end
