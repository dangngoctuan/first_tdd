class Article < ActiveRecord::Base
  has_many :texts, dependent: :destroy
  has_many :images, dependent: :destroy
  validates :title, :article_likes, presence: true

  def by_search
    self.texts + self.images
  end

  def by_published
    self.published
  end

  def by_published_day
    self.published_day < Time.now
  end

  def by_public
    self.to_s
  end

  def increment
    self.article_likes
  end
end
