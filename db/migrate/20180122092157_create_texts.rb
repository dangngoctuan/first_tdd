class CreateTexts < ActiveRecord::Migration[5.1]
  def change
    create_table :texts do |t|
      t.string :text_head_line
      t.integer :text_menu
      t.string :message
      t.integer :text_likes
    end
  end
end
